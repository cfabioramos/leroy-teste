package com.teste.leroy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class LeroyApplication {

	public static void main(String[] args) {
		SpringApplication.run(LeroyApplication.class, args);
	}

}
