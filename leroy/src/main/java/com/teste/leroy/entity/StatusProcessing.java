package com.teste.leroy.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class StatusProcessing {
	
	public StatusProcessing() {
	}
	
	public StatusProcessing(String fileName) {
		this.fileName = fileName;
		this.isOk = true;
	}
	
	public StatusProcessing(String fileName, Boolean isOk) {
		this.fileName = fileName;
		this.isOk = isOk;
	}

	@Id
	private String fileName;
	
	private Boolean isOk;
	
	private LocalDateTime dateTime = LocalDateTime.now();

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Boolean getIsOk() {
		return isOk;
	}

	public void setIsOk(Boolean isOk) {
		this.isOk = isOk;
	}
	
	public LocalDateTime getDateTime() {
		return dateTime;
	}
	
	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}
	
}
