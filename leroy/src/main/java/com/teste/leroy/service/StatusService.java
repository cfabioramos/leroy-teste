package com.teste.leroy.service;

import java.util.Optional;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teste.leroy.entity.StatusProcessing;
import com.teste.leroy.repository.StatusRepository;

@Service
public class StatusService {

	@Autowired
    private StatusRepository repository;
	
	public Optional<StatusProcessing> findById(@PathParam("id") String fileName){
		return repository.findById(fileName);
	}
	
	private StatusProcessing save(StatusProcessing statusProcessing) {
		return this.repository.save(statusProcessing);
	}

	public void saveOk(String fileName) {
		this.save(new StatusProcessing(fileName));
	}
	
	public void saveNoOk(String fileName) {
		this.save(new StatusProcessing(fileName, false));
	}
	
}
