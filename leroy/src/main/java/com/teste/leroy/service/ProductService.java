package com.teste.leroy.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.teste.leroy.entity.Category;
import com.teste.leroy.entity.Product;
import com.teste.leroy.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ProductRepository repository;

	@Autowired
	private StatusService statusService;

	public List<Product> findAll() {
		return repository.findAll();
	}

	public Optional<Product> findById(Long id) {
		return repository.findById(id);
	}

	public Product save(Product product) {
		return this.repository.save(product);
	}

	@Async
	public List<Product> save(MultipartFile excelDataFile) throws IOException {

		List<Product> productList = this.getProductsFromSheet(excelDataFile);
		List<Product> saved = this.repository.saveAll(productList);

		statusService.saveOk(excelDataFile.getOriginalFilename());

		return saved;
	}

	@SuppressWarnings("resource")
	public List<Product> getProductsFromSheet(MultipartFile excelDataFile) throws IOException {

		Workbook workbook = null;

		String lowerCaseFileName = excelDataFile.getOriginalFilename().toLowerCase();
		if (lowerCaseFileName.endsWith(".xlsx")) {
			workbook = new XSSFWorkbook(excelDataFile.getInputStream());
		} else {
			workbook = new HSSFWorkbook(excelDataFile.getInputStream());
		}

		Sheet worksheet = workbook.getSheetAt(0);
		return extractProductListFromSheet(worksheet, this.extractCategoryFromSheet(worksheet));
	}

	private List<Product> extractProductListFromSheet(Sheet worksheet, Category category) {
		List<Product> productList = new ArrayList<Product>();
		int productLineControl = 3;
		while (true) {
			
			Row row = worksheet.getRow(productLineControl++);
			if (row == null)
				break;
			
			Cell firstCell = row.getCell(0);
			if (firstCell == null || firstCell.getCellTypeEnum() == CellType.BLANK) {
				break;
			}

			Product product = new Product();
			product.setIm(Double.valueOf(firstCell.getNumericCellValue()).intValue());
			product.setName(row.getCell(1).getStringCellValue());
			product.setFreeShipping(row.getCell(2).getNumericCellValue() == 1.0 ? true : false);
			product.setDescription(row.getCell(3).getStringCellValue());
			product.setPrice(Double.valueOf(row.getCell(4).getStringCellValue()));
			product.setCategory(category);
			
			productList.add(product);
		}
		return productList;
	}

	private Category extractCategoryFromSheet(Sheet worksheet) {

		Row row = worksheet.getRow(0);
		Cell categoryFromSheet = row.getCell(1);
		
		if (categoryFromSheet == null)
			throw new IllegalArgumentException("The Category can not be empty");
		
		String categoryName = null;
		
		try {
			categoryName = String.valueOf(Double.valueOf(categoryFromSheet.getNumericCellValue()).intValue());
		}
		catch (Exception e) {
			throw new IllegalArgumentException("The informed Category is invalid");
		}

		if (categoryName != null && !"".equals(categoryName)) {
			if (!this.categoryService.findById(categoryName).isPresent())
				throw new IllegalArgumentException("The given category does not exists");
		}

		return new Category(categoryName != null ? String.valueOf(categoryName) : "");

	}

	public Product update(Long id, Product product) {
		return repository.findById(id).map(record -> {
			record.setName(product.getName());
			record.setDescription(product.getDescription());
			record.setPrice(product.getPrice());
			record.setFreeShipping(product.getFreeShipping());
			Product updated = repository.save(record);
			return updated;
		}).orElse(null);
	}

	public void deleteById(Long id) {
		this.repository.deleteById(id);
	}

}
