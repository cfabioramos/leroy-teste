package com.teste.leroy.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teste.leroy.entity.Category;
import com.teste.leroy.repository.CategoryRepository;

@Service
public class CategoryService {

	@Autowired
    private CategoryRepository repository;
	
	public Optional<Category> findById(String name){
		return repository.findById(name);
	}
	
}
