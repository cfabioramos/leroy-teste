package com.teste.leroy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.teste.leroy.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{

}
