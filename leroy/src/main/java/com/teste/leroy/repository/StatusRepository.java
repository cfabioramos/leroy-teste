package com.teste.leroy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.teste.leroy.entity.StatusProcessing;

public interface StatusRepository extends JpaRepository<StatusProcessing, String>{

}
