package com.teste.leroy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.teste.leroy.entity.Category;

public interface CategoryRepository extends JpaRepository<Category, String> {

}
