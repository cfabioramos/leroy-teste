package com.teste.leroy.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.teste.leroy.entity.StatusProcessing;
import com.teste.leroy.service.StatusService;

@RestController
@RequestMapping("/status")
public class StatusResource {
	
	@Autowired
    private StatusService service;

	@GetMapping("/{originalFileName}")
	public StatusProcessing findById(@PathVariable("originalFileName") String fileName){
		return service.findById(fileName).get();
	}
	
}
