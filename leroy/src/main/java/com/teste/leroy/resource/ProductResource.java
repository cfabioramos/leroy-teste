package com.teste.leroy.resource;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.teste.leroy.entity.Product;
import com.teste.leroy.service.ProductService;

@RestController
@RequestMapping("/products")
public class ProductResource {
	
	@Autowired
    private ProductService service;
	
	@PostMapping(value = "/upload", consumes = "multipart/form-data")
	public ResponseEntity<?> save(@RequestParam("file") MultipartFile excelDataFile) throws IOException {
		return ResponseEntity.ok().body(service.save(excelDataFile));
	}
	
	@PutMapping(path ={"/{id}"})
	public ResponseEntity<Product> update(
			@PathVariable("id") Long id, @RequestBody Product product) {
		
	   return service.findById(id)
	           .map(record -> {
	               record.setName(product.getName());
	               record.setDescription(product.getDescription());
	               record.setPrice(product.getPrice());
	               record.setFreeShipping(product.getFreeShipping());
	               Product updated = service.save(record);
	               return ResponseEntity.ok().body(updated);
	           }).orElse(ResponseEntity.notFound().build());
	}
	
	@GetMapping
	public List<Product> findAll(){
		return service.findAll();
	}
	
	@GetMapping("/{id}")
	public Product findById(@PathVariable("id") Long id){
		return service.findById(id).get();
	}
	
	@DeleteMapping(path ={"/{id}"})
	public ResponseEntity<?> delete(@PathVariable Long id) {
		return service.findById(id)
				.map(record -> {
					service.deleteById(id);
					return ResponseEntity.ok().build();
				}).orElse(ResponseEntity.notFound().build());
	}
	
}
