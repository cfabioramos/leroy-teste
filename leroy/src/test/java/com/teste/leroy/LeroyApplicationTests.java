package com.teste.leroy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;

import com.teste.leroy.entity.Category;
import com.teste.leroy.entity.Product;
import com.teste.leroy.service.CategoryService;
import com.teste.leroy.service.ProductService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LeroyApplicationTests {
	
	private String XLS_CONTENT_TYPE = "application/vnd.ms-excel";
	private String XLSX_CONTENT_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

	@Mock
	private CategoryService categoryService;
	
	@InjectMocks
	@Resource
	private ProductService service;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void validateProductsBasic() throws IOException {
		
		MockMultipartFile firstFile = new MockMultipartFile(
				"data_test_1", "products_test_1.xlsx", XLSX_CONTENT_TYPE, 
				this.getClass().getClassLoader().getResourceAsStream("com/teste/leroy/products_test_1.xlsx"));
		
		when(categoryService.findById("123123")).thenReturn(Optional.of(new Category("123123")));
		List<Product> productsFromService = service.getProductsFromSheet(firstFile);
		
		List<Product> productsToTest = new ArrayList<Product>();
		
		Product product1 = new Product();
		product1.setIm(1001);
		product1.setName("Furadeira X");
		product1.setCategory(new Category("123123"));
		productsToTest.add(product1);
		
		Product product2 = new Product();
		product2.setIm(1002);
		product2.setName("Furadeira Y");
		product2.setCategory(new Category("123123"));
		productsToTest.add(product2);
		
		assertEquals(productsToTest, productsFromService);
	}
	
	@Test(expected = IllegalArgumentException.class) 
	public void failCategoryNotFound() throws IOException {
		
		MockMultipartFile secondFile = new MockMultipartFile(
				"data_test_2", "products_test_category_not_found.xlsx", XLSX_CONTENT_TYPE, 
				this.getClass().getClassLoader().getResourceAsStream("com/teste/leroy/products_test_category_not_found.xlsx"));
		
		when(categoryService.findById("222234")).thenReturn(Optional.ofNullable(null));
		service.getProductsFromSheet(secondFile);
		
	}
	
	@Test
	public void failInvalidCategory() throws IOException {
		
		MockMultipartFile thirdFile = new MockMultipartFile(
				"data_test_3", "invalid_category_test.xlsx", XLSX_CONTENT_TYPE,
				this.getClass().getClassLoader().getResourceAsStream("com/teste/leroy/invalid_category_test.xlsx"));
		
		try {
			service.getProductsFromSheet(thirdFile);
		}
		catch (IllegalArgumentException e) {
			String exceptionMessage = e.getMessage();
			assertThat(exceptionMessage, 
					org.hamcrest.CoreMatchers.either(
							org.hamcrest.CoreMatchers.containsString("The Category can not be empty"))
					.or(org.hamcrest.CoreMatchers.containsString("The informed Category is invalid")));
			
		}
		
	}
	
}
